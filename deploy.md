# Fibric Setup

## Change ubuntu 20.04 sources

- 查看版本

```
lsb_release -c
```

- 备份原源

```
sudo cp -v /etc/apt/sources.list /etc/apt/sources.list.backup
```

- 修改编辑权限

```
sudo chmod 777 /etc/apt/sources.list
```

- 编辑源

```
sudo gedit /etc/apt/sources.list
或
vim /etc/apt/sources.list

```

- 阿里源

  21.10:impish / 21.04:hirsute / 20.10:groovy / 20.04:focal / 18.04:bionic / 16.04:xenial / 14.04:trusty

```
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
```

- 清华源

```
https://mirrors.tuna.tsinghua.edu.cn/ubuntu/
```

- 163 源

```
http://mirrors.163.com/ubuntu/
```

- 更新源

```
sudo apt update
sudo apt upgrade
```

## Install curl

```
sudo apt install -y curl
```

## Install git

```
sudo apt install -y git
```

## Install Golang

- 下载

```
curl -O -L https://golang.google.cn/dl/go1.17.5.linux-amd64.tar.gz
```

- 解压

```
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.5.linux-amd64.tar.gz
```

- 设置 PATH  
  $HOME/.profile or /etc/profile

```
sudo vim ~/.profile
export PATH=$PATH:/usr/local/go/bin
source ~/.profile
go version
```

- 设置源

```
go env -w GOPROXY=https://goproxy.cn,direct
export GOPROXY=https://goproxy.cn
```

## Install Docker

- Uninstall old versions

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

### Install using the repository

- Install dep

```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

- Add official GPG

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

- Set up the repository

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

- Install Docker Engine

```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Install using pkg

```
sudo apt install docker
```

### Install using script

```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

### After Install

- 添加 docker 用户组

```
# step 1: 创建docker用户组
sudo groupadd docker
# step 2:将当前用户添加到docker用户组
sudo usermod -aG docker $USER
```

- 修改 docker 源

```
vim /etc/docker/daemon.json

{
	"registry-mirrors": [
		"https://hub-mirror.c.163.com"
	]
}

```

- 重启服务

```
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl restart docker

docker -v
docker info
```

## Install Docker-Compose

```
sudo curl -L https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose -v
```

## Fabric 的环境搭建

- mkdir

```
mkdir -p ~/go/src/github.com/hyperledger
cd ~/go/src/github.com/hyperledger
```

- download

```
curl -sSL https://bit.ly/2ysbOFE | bash -s

or
curl -sSL https://gitlab.com/0bug/fabric/-/raw/main/bootstrap.sh | bash -s

or
curl -sSL https://bit.ly/2ysbOFE | bash -s -- <fabric_version> <fabric-ca_version>
curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.2.0 1.4.7

or
git clone git://github.com/hyperledger/fabric.git
cd fabric/scripts
sudo ./bootstrap.sh

or
wget https://github.com/hyperledger/fabric/blob/master/scripts/bootstrap.sh
chmod +x bootstrap.sh
./bootstrap.sh 2.4.0

```

- set PATH

```
vim ~/.bashrc
export PATH=$HOME/fabric/fabric-samples/bin:$PATH
source ~/.bashrc
orderer version
```

- 测试网络

- 启动 / 关闭

```
cd fabric-samples/test-network
./network.sh -h
./network.sh up
./network.sh down
```

## 基于 Raft 排序的 Fabric2.3 多机集群部署教程

### create network

- host-1

```
docker swarm init --advertise-addr <host-1 ip address>
docker swarm join-token manager

docker network create --attachable --driver overlay  fabric
docker network ls
```

- set node label

```
#manger节点上执行
docker node update v5nh3mlnkmhdeain8w63o8f3d --label-add fabric_node_01=fabric_node_01
docker node update arcxa5tm4ap9ki3nkdkvxdunn --label-add fabric_node_02=fabric_node_02
docker node update tf6h729w261zmagdmo73pq1eo --label-add fabric_node_03=fabric_node_03
docker node update nv2jgx3c5zkgf3owgvqay1qtl --label-add fabric_node_04=fabric_node_04
#v5nh3mlnkmhdeain8w63o8f3d等是node的id，docker node ls能看到
#上面为什么fabric_node_01=fabric_node_01(即key=value)每个的key不一样，因为这样就可以后面的容器可以指定要一个节点或多个节点（这个就可以把fabric布到一个节点或多机节点，开发和测试都可以用一套配置了），如果key相同则不能做到单节点布署
```

- host1-4

```
firewall-cmd --add-port=2376/tcp --permanent
firewall-cmd --add-port=2377/tcp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=4789/udp --permanent

iptables -I INPUT -p tcp -m tcp --dport 2376 -j ACCEPT
iptables -I INPUT -p tcp -m tcp --dport 2377 -j ACCEPT
iptables -I INPUT -p tcp -m tcp --dport 7946 -j ACCEPT
iptables -I INPUT -p tcp -m udp --dport 7946 -j ACCEPT
iptables -I INPUT -p tcp -m udp --dport 4789 -j ACCEPT

sudo netfilter-persistent save
sudo netfilter-persistent reload
```

```
#上一章用了这章再用一下
#>netstat -ntlp|grep docker
tcp6       0      0 :::7050                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7946                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7051                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7053                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7150                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7151                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7153                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7251                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7253                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7351                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::7353                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::2375                 :::*                    LISTEN      22423/dockerd
tcp6       0      0 :::9000                 :::*                    LISTEN      23793/docker-proxy
tcp6       0      0 :::2377                 :::*                    LISTEN      22423/dockerd
#>firewall-cmd --permanent --add-port={9000/tcp,9000/udp}
#这里只取了其中一个，要把所有的相关端口全开了
#重启防火墙
#>firewall-cmd --reload

firewalld的基本使用
启动： systemctl start firewalld
查状态：systemctl status firewalld
停止： systemctl disable firewalld
禁用： systemctl stop firewalld
在开机时启用一个服务：systemctl enable firewalld.service
在开机时禁用一个服务：systemctl disable firewalld.service
查看服务是否开机启动：systemctl is-enabled firewalld.service
查看已启动的服务列表：systemctl list-unit-files|grep enabled
查看启动失败的服务列表：systemctl --failed

查看版本： firewall-cmd --version
查看帮助： firewall-cmd --help
显示状态： firewall-cmd --state
查看所有打开的端口： firewall-cmd --zone=public --list-ports
更新防火墙规则： firewall-cmd --reload
查看区域信息: firewall-cmd --get-active-zones
查看指定接口所属区域： firewall-cmd --get-zone-of-interface=eth0
拒绝所有包：firewall-cmd --panic-on
取消拒绝状态： firewall-cmd --panic-off
查看是否拒绝： firewall-cmd --query-panic

```

- host2-4

```
docker swarm join --token <manager-token> <host-1 ip>:2377
docker network ls
```

### create profile

```
cd fabric-samples
mkdir raft-4node-swarm
cd raft-4node-swarm

cryptogen showtemplate > crypto-config.yaml

edit crypto-config.yaml
edit configtx.yaml

```

```
1.生成创世区块
cryptogen generate --config=crypto-config.yaml

2.生成通道配置信息
configtxgen -profile SampleMultiNodeEtcdRaft -channelID fabric-cluster-channel -outputBlock ./channel-artifacts/genesis.block


3.创建通道配置信息
configtxgen -profile Org1Channel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID mychannel

4. 为org1配置锚节点
configtxgen -profile Org1Channel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID mychannel -asOrg Org1MSP

```

### copy raft-example

```
scp -r raft-example root@host2-4:/root/raft-example
```

### copy host file

### start docker

- host1-4

```
chmod +x up.sh
chmod +x down.sh

vi /etc/hosts

172.16.80.64 orderer0.example.com peer0.org1.example.com couchdb0
172.16.80.65 orderer1.example.com peer1.org1.example.com
172.16.80.66 orderer2.example.com peer2.org1.example.com
172.16.80.67 peer3.org1.example.com

systemctl restart network
```

### copy chaincode file

#### host1

- 进入 cli 容器

> 默认为 cli 的工作目录为/opt/gopath/src/github.com/hyperledger/fabric/peer

```
docker exec -it cli2 bash
```

- 创建通道，在当前目录下生成通道块文件

```
peer channel create -o orderer0.example.com:7050 -c mychannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

- 让当前节点 peer0.org1.example.com 加入到通道中

```
peer channel join -b mychannel.block
```

- 为 Org1 组织更新锚节点

```
peer channel update -o orderer0.example.com:7050 -c mychannel -f ./channel-artifacts/Org1MSPanchors.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

- 打包链码

```
peer lifecycle chaincode package mycc.tar.gz --path /opt/gopath/src/github.com/chaincode/ --lang golang --label 1.0
```

- 安装链码

```
peer lifecycle chaincode install mycc.tar.gz
```

- 获取 package_id

```
#查看当前节点安装链码的package_id，并将返回结果放入到当前log.txt中
peer lifecycle chaincode queryinstalled>&log.txt
cat log.txt
使用sed通过正则表达式对log.txt中内容进行截取得到package_id并放到环境变量中，后面通过$PACKAGE_ID就可以获取到该值
PACKAGE_ID=$(sed -n "{s/^Installed chaincodes on peer:.*$//; s/Package ID: //; s/, Label:.*$//; p;}" log.txt)
```

- 为当前节点所在的组织 Org1 提出链码审批

```
peer lifecycle chaincode approveformyorg --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name mycc --version 1.0 --init-required --package-id $PACKAGE_ID --sequence 1 --waitForEvent


执行成功后查看
peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name mycc --version 1.0 --sequence 1 --output json --init-required
```

- mycc.tar.gz\mychannel.block 拷贝到其他主机

```
docker cp /opt/gopath/src/github.com/hyperledger/fabric/peer/ /root/fabric/
scp -r /root/fabric/peer/ root@172.16.80.65:/root/raft-example
```

#### host2-4

- mycc.tar.gz\mychannel.block 拷贝到主机

```
docker cp /root/raft-example/peer/ cli2:/opt/gopath/src/github.com/hyperledger/fabric/peer/
```

- 进入 cli 容器

```
docker exec -it cli2 bash
```

- 拉取通道块

```
peer channel fetch config>mychannel.block -c mychannel -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

- 加入通道，打包链码，安装链码

```
#node join channel
peer channel join -b mychannel.block
#package chaincode for node / or copy from host1
peer lifecycle chaincode package mycc.tar.gz --path /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/ --lang golang --label 1.0
#install chaincode for node
peer lifecycle chaincode install mycc.tar.gz
```

#### 任意 host cli

- 链码容器启动

```
peer lifecycle chaincode commit -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --version 1.0 --sequence 1 --init-required
```

- 初始化链码

```
peer chaincode invoke -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --isInit -c '{"Args":["Init","a","100","b","200"]}'
```

- 对链码进行一次 invoke 的操作，从 a 向 b 转账 10 块钱

```
peer chaincode invoke -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'
```

- 对链码进行一次 query 的操作，查询 a 现在还剩下多少钱

```
peer chaincode query -C mychannel -n mycc -c '{"Args":["query","a"]}'
```

## cmd

```
停止所有container
docker stop $(docker ps -a -q)

删除所有container
docker rm $(docker ps -a -q)

删除所有images
docker rmi -f $(docker images -q)

IPV4转发
vi /usr/lib/sysctl.d/00-system.conf

net.ipv4.ip_forward=1

systemctl restart network
```
